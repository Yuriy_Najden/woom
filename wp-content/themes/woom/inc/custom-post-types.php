<?php


    /**
     * Register a auto examples post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @since woom 1.0
     */

    function auto_examples_post_type() {

        $labels = array(
            'name'               => _x( 'Примеры авто', 'post type general name', 'woom' ),
            'singular_name'      => _x( 'Пример авто', 'post type singular name', 'woom' ),
            'menu_name'          => _x( 'Примеры авто', 'admin menu', 'woom' ),
            'name_admin_bar'     => _x( 'Примеры авто', 'add new on admin bar', 'woom' ),
            'add_new'            => _x( 'Добавить новоый пример авто', 'actions', 'woom' ),
            'add_new_item'       => __( 'Добавить новоый пример авто', 'woom' ),
            'new_item'           => __( 'Новый пример авто', 'woom' ),
            'edit_item'          => __( 'Редактировать пример авто', 'woom' ),
            'view_item'          => __( 'Посмотреть пример авто', 'woom' ),
            'all_items'          => __( 'Всё примеры авто', 'woom' ),
            'search_items'       => __( 'Искать пример авто', 'woom' ),
            'parent_item_colon'  => __( 'Родитель пример авто:', 'woom' ),
            'not_found'          => __( 'Примеров авто не найдено.', 'woom' ),
            'not_found_in_trash' => __( 'В корзине примеров авто не найдено.', 'woom' )
        );

        $args = array(
            'labels'             => $labels,
            'taxonomies'         => array('auto-examples-tax', 'post_tag'),
            'description'        => __( 'Описание.', 'woom' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'auto_examples' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => 6,
            'menu_icon'          => 'dashicons-format-aside',
            'supports'           => array( 'title', 'thumbnail' )
        );

        register_post_type( 'auto_examples', $args );
    }

    add_action( 'init', 'auto_examples_post_type' );

    add_action( 'init', 'create_auto_examples_taxonomy' );
    function create_auto_examples_taxonomy(){

        register_taxonomy('auto-examples-tax', 'auto_examples', array(
            'label'                 => 'auto-examples-tax', // определяется параметром $labels->name
            'labels'                => array(
                'name'              => 'Тип кузова',
                'singular_name'     => 'Кузов',
                'search_items'      => 'Поиск кузова',
                'all_items'         => 'Все кузова',
                'view_item '        => 'View Genre',
                'parent_item'       => 'Parent Genre',
                'parent_item_colon' => 'Parent Genre:',
                'edit_item'         => 'Редактировать кузов',
                'update_item'       => 'Обновить кузов',
                'add_new_item'      => 'Добавить новый кузов',
                'new_item_name'     => 'New Genre Name',
                'menu_name'         => 'Типы кузовов',
            ),
            'description'           => 'auto-examples-tax', // описание таксономии
            'public'                => true,
            'publicly_queryable'    => true, // равен аргументу public
            'show_in_nav_menus'     => true, // равен аргументу public
            'show_ui'               => true, // равен аргументу public
            'show_in_menu'          => true, // равен аргументу show_ui
            'show_tagcloud'         => true, // равен аргументу show_ui
            'show_in_rest'          => true, // добавить в REST API
            'rest_base'             => true, // $taxonomy
            'hierarchical'          => true,

            /*'update_count_callback' => '_update_post_term_count',*/
            'rewrite'               => array('slug' => 'auto_examples'),
            'query_var'             => $taxonomy, // название параметра запроса
            'capabilities'          => array(),
            'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
            'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
            /*'_builtin'              => false,*/
            'show_in_quick_edit'    => true, // по умолчанию значение show_ui
        ) );
    }

    /**
     * Register a delivered cars post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @since woom 1.0
     */

    function delivered_cars_post_type() {

        $labels = array(
            'name'               => _x( 'Доставленные авто', 'post type general name', 'woom' ),
            'singular_name'      => _x( 'Доставленные авто', 'post type singular name', 'woom' ),
            'menu_name'          => _x( 'Доставленные авто', 'admin menu', 'woom' ),
            'name_admin_bar'     => _x( 'Доставленные авто', 'add new on admin bar', 'woom' ),
            'add_new'            => _x( 'Добавить новое доставленное авто', 'actions', 'woom' ),
            'add_new_item'       => __( 'Добавить новое доставленное авто', 'woom' ),
            'new_item'           => __( 'Новое доставленное авто', 'woom' ),
            'edit_item'          => __( 'Редактировать доставленное авто', 'woom' ),
            'view_item'          => __( 'Посмотреть доставленное авто', 'woom' ),
            'all_items'          => __( 'Всё доставленные авто', 'woom' ),
            'search_items'       => __( 'Искать доставленное авто', 'woom' ),
            'parent_item_colon'  => __( 'Родитель доставленное авто:', 'woom' ),
            'not_found'          => __( 'Доставленных авто не найдено.', 'woom' ),
            'not_found_in_trash' => __( 'В корзине доставленных авто не найдено.', 'woom' )
        );

        $args = array(
            'labels'             => $labels,
            'taxonomies'         => [],
            'description'        => __( 'Описание.', 'woom' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'delivered_cars' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => 7,
            'menu_icon'          => 'dashicons-format-gallery',
            'supports'           => array( 'title', 'thumbnail' )
        );

        register_post_type( 'delivered_cars', $args );
    }

    add_action( 'init', 'delivered_cars_post_type' );