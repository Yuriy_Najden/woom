<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "woom_option";

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Site Options', 'woom' ),
        'page_title'           => __( 'Site Options', 'woom' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => 'AIzaSyBwXsRkKFfR0vY-BkQv5mRCa0twR46KzAY',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => false,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-settings',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    //
    Redux::setArgs( $opt_name, $args );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Главные', 'woom' ),
        'id'               => 'main-options',
        'fields'           => array(

            array(
                'id'       => 'fb_link',
                'type'     => 'text',
                'title'    => __('Ссылка на Facebook', 'woom'),
                'validate' => 'url',
                'msg'      => 'Укажите ссылку',
            ),
            array(
                'id'       => 'inst_link',
                'type'     => 'text',
                'title'    => __('Ссылка на Instagram', 'woom'),
                'validate' => 'url',
                'msg'      => 'Укажите ссылку',
            ),
            array(
                'id'       => 'email_address',
                'type'     => 'text',
                'title'    => __('Email адрес', 'woom'),
                'validate' => 'email',
                'msg'      => 'Укажите ссылку',
            ),
            array(
                'id'       => 'phones',
                'type'     => 'multi_text',
                'title'    => __( 'Контактные телефоны', 'woom' ),
            ),
            array(
                'id'       => 'viber_link',
                'type'     => 'text',
                'title'    => __('Ссылка на viber', 'woom'),
            ),
            array(
                'id'       => 'telegram_link',
                'type'     => 'text',
                'title'    => __('Ссылка на telegram', 'woom'),
                'validate' => 'url',
                'msg'      => 'Укажите ссылку telegram',
            ),
            array(
                'id'       => 'whatsapp_link',
                'type'     => 'text',
                'title'    => __('Ссылка на WhatsApp', 'woom'),
                'validate' => 'url',
                'msg'      => 'Укажите ссылку WhatsApp',
            ),
            array(
                'id'       => 'fb_mess_link',
                'type'     => 'text',
                'title'    => __('Ссылка на fb месенджер', 'woom'),
                'msg'      => 'Укажите ссылку fb месенджер',
            ),

            array(
                'id'         => 'privacy_policy',
                'type'       => 'media',
                'title'      => __( 'Privacy Policy', 'woom' ),
                'full_width' => true,
                'mode'       => false,
                // Can be set to false to allow any media type, or can also be set to any mime type.
                /*'desc'       => __( 'Basic media uploader with disabled URL input field.', 'sherp' ),
                'subtitle'   => __( 'Upload any media using the WordPress native uploader', 'sherp' ),*/
            ),
            array(
                'id'       => 'min_price',
                'type'     => 'text',
                'title'    => __('Минимальная стоимость автомобиля в долларах', 'woom'),
                'validate' => 'numeric',
            ),

            array(
                'id'       => 'max_price',
                'type'     => 'text',
                'title'    => __('Максимальная стоимость автомобиля в долларах', 'woom'),
                'validate' => 'numeric',
            ),

        )
    ));

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Переводы статических элементов', 'woom' ),
        'id'               => 'translations-options',
        'fields'           => array(
            array(
                'id'       => 'copy_text_ru',
                'type'     => 'text',
                'title'    => __('Текст копирайта', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'copy_text_ua',
                'type'     => 'text',
                'title'    => __('Текст копирайта', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'privacy_text_ru',
                'type'     => 'text',
                'title'    => __('Текст кнопки "политика конфиденциальности"', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'privacy_text_ua',
                'type'     => 'text',
                'title'    => __('Текст кнопки "политика конфиденциальности"', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'form_name_ru',
                'type'     => 'text',
                'title'    => __('Заголовок формы обратной связи', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'form_name_ua',
                'type'     => 'text',
                'title'    => __('Заголовок формы обратной связи', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'input_name_ru',
                'type'     => 'text',
                'title'    => __('Подпись поля формы "Имя"', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'input_name_ua',
                'type'     => 'text',
                'title'    => __('Подпись поля формы "Имя"', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'input_tel_ru',
                'type'     => 'text',
                'title'    => __('Подпись поля формы "Телефон"', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'input_tel_ua',
                'type'     => 'text',
                'title'    => __('Подпись поля формы "Телефон"', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'input_message_ru',
                'type'     => 'text',
                'title'    => __('Подпись поля формы "Сообщение"', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'input_message_ua',
                'type'     => 'text',
                'title'    => __('Подпись поля формы "Сообщение"', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'input_button_ru',
                'type'     => 'text',
                'title'    => __('Текст кнопки отправки формы', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'input_button_ua',
                'type'     => 'text',
                'title'    => __('Текст кнопки отправки формы', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'saving_slide_ru',
                'type'     => 'text',
                'title'    => __('Подпись "Экономия" в слайдере на главном экране', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'saving_slide_ua',
                'type'     => 'text',
                'title'    => __('Подпись "Экономия" в слайдере на главном экране', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'price_range_ru',
                'type'     => 'text',
                'title'    => __('Подпись "Цена" в фильтации', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'price_range_ua',
                'type'     => 'text',
                'title'    => __('Подпись "Цена" в фильтации', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),

            array(
                'id'       => 'contact_name_ru',
                'type'     => 'text',
                'title'    => __('Подпись "Контакты"', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'contact_name_ua',
                'type'     => 'text',
                'title'    => __('Подпись "Контакты"', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),

            array(
                'id'       => 'we_map_ru',
                'type'     => 'text',
                'title'    => __('Подпись "Мы на карте"', 'woom'),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'we_map_ua',
                'type'     => 'text',
                'title'    => __('Подпись "Мы на карте"', 'woom'),
                'desc'     => __('Украинский текс', 'woom'),
            ),

        )
    ));

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Подписи технических характеристик авто', 'woom' ),
        'id'               => 'car-technical-characteristics',
        'fields'           => array(
            array(
                'id'       => 'car_model_ru',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Марка авто"', 'woom' ),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'car_model_ua',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Марка авто"', 'woom' ),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'car_year_ru',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Год выпуска"', 'woom' ),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'car_year_ua',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Год выпуска"', 'woom' ),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'car_engine_volume_ru',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Объем двигателя"', 'woom' ),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'car_engine_volume_ua',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Объем двигателя"', 'woom' ),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'car_engine_fuel_ru',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Топливо"', 'woom' ),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'car_engine_fuel_ua',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Топливо"', 'woom' ),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'car_engine_power_ru',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Мощность двигателя"', 'woom' ),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'car_engine_power_ua',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Мощность двигателя"', 'woom' ),
                'desc'     => __('Украинский текс', 'woom'),
            ),
            array(
                'id'       => 'car_cost_ru',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Диапазон цен с учетом растаможки"', 'woom' ),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'car_cost_ua',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Диапазон цен с учетом растаможки"', 'woom' ),
                'desc'     => __('Украинский текс', 'woom'),
            ),

            array(
                'id'       => 'car_cost_inua_ru',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Цена за авто в Украине"', 'woom' ),
                'desc'     => __('Русский текс', 'woom'),
            ),
            array(
                'id'       => 'car_cost_inua_ua',
                'type'     => 'text',
                'title'    => __( 'Назание поля "Цена за авто в Украине"', 'woom' ),
                'desc'     => __('Украинский текс', 'woom'),
            ),




        )
    ));

