<?php

    function woom_scripts() {

        // Styles

        wp_enqueue_style( 'woom-style', get_stylesheet_uri(), array(), _S_VERSION );

        wp_style_add_data( 'woom-style', 'rtl', 'replace' );

        wp_enqueue_style( 'woom-main-styles', get_template_directory_uri() . '/assets/css/style.css', array(), _S_VERSION );

        // Scripts

        wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), 4, true );

        wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/js/slick.min.js', array('jquery'), 1.6 , true );

        wp_enqueue_script( 'masked', get_template_directory_uri() . '/assets/js/jquery.maskedinput.min.js', array('jquery'), 1.4 , true );

        wp_enqueue_script( 'viewport', get_template_directory_uri() . '/assets/js/jquery.viewportchecker.js', array('jquery'), 1.4 , true );

        wp_enqueue_script( 'lazy', get_template_directory_uri() . '/assets/js/jquery.lazy.min.js', array('jquery'), 0.14 , true );

        wp_enqueue_script( 'odometr', get_template_directory_uri() . '/assets/js/odometer.js', array('jquery'), 1 , true );

        wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js', array('jquery'), 1 , true );

        wp_enqueue_script( 'googlemap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBwXsRkKFfR0vY-BkQv5mRCa0twR46KzAY', array('jquery'), '1.0', true );

        wp_enqueue_script( 'woom-main-js', get_template_directory_uri() . '/assets/js/js.js', array('jquery'), _S_VERSION, true );

        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
    }
    add_action( 'wp_enqueue_scripts', 'woom_scripts' );