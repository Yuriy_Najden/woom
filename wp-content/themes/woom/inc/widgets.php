<?php

    function woom_widgets_init() {
        register_sidebar(
            array(
                'name'          => esc_html__( 'Языковая панель', 'woom' ),
                'id'            => 'lang-sidebar',
                'description'   => esc_html__( 'Добавте виджет сюда', 'woom' ),
                'before_widget' => '<div class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            )
        );
    }
    add_action( 'widgets_init', 'woom_widgets_init' );