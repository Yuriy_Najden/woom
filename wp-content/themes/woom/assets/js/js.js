jQuery(function() {

    var WinWid = jQuery(window).width();
    var winHeid = jQuery(window).height();

    // Menu open

    jQuery('#menu-btn').on('click', function (e) {
       e.preventDefault();

       jQuery(this).toggleClass('close-menu');

       jQuery('#navigation').toggleClass('open-menu');

    });

    jQuery('#navigation .inner .main-menu a, #navigation .inner .mess-wrapper, main').on('click', function (e) {
       // e.preventDefault();

        jQuery('#navigation').removeClass('open-menu');
        jQuery('#menu-btn').removeClass('close-menu');
    });

    if( WinWid <= 1440 ){
        jQuery('header .container-fluid').addClass('container');
        jQuery('header .container-fluid').removeClass('container-fluid');
    }


    // SCROLL MENU

    jQuery('#main-menu li a').addClass('scroll-to');

    jQuery(document).on('click', '.scroll-to', function (e) {
        e.preventDefault();

        var href = jQuery(this).attr('href');

        jQuery('html, body').animate({
            scrollTop: jQuery(href).offset().top
        }, 1000);

    });

    // Menu fixed

    jQuery(document).scroll(function() {
        var y = jQuery(this).scrollTop();

        var stepPosition = jQuery('#steps .content').offset();

        stepPosition = stepPosition.top;


        if( WinWid > 1200){
            stepPosition = stepPosition - 500;
        }
        if( WinWid <= 1200){
            stepPosition = stepPosition - 400;
        }
        if( WinWid <= 575 ){

            stepPosition = stepPosition - 10;

        }

        if (y > 1) {
            jQuery('header').addClass('fixed');
        } else {
            jQuery('header').removeClass('fixed');
        }

        if( y >= stepPosition ){

            var animateDiwn = y - stepPosition;

            jQuery('.buy-auto-simple .content .animatr-line span').css({'transform' : 'translateY('+animateDiwn+'px)'});
        } else{
            jQuery('.buy-auto-simple .content .animatr-line span').css({'transform' : 'translateY(0px)'});
        }

        /*var bSIT1 = jQuery('.responsibility').offset();
        var bSIB1 = jQuery('.buy-auto-simple').offset();

        bSIT1 = bSIT1.top;
        bSIB1 = bSIB1.top;

        var arrIconPosition = jQuery('header');

        jQuery.each(arrIconPosition, function( index, element){

            var socElement = jQuery(element);

            var socPosition = jQuery(element).offset().top;

            if( socPosition >= bSIT1 && socPosition <= bSIB1 ) {

                socElement.addClass('hidden');

            } else {
                socElement.removeClass('hidden');
            }
        });*/
    });

    var position = jQuery(window).scrollTop();

    // should start at 0

    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();
        if(scroll > position) {

            jQuery('header').removeClass('fixed-vis');

        } else {
            jQuery('header').addClass('fixed-vis');

        }
        position = scroll;
    });

    // PHONE MASK

    jQuery("input[type=tel]").mask("+38(999) 999-99-99");

    // Prity num

    /*jQuery('#costRange').on('mousedown', function () {
        setInterval(function () {
            var num = jQuery('#priceOutputId').val();
            var newNum = num.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' ');
            jQuery('#priceOutputId').text(newNum);
        }, 1);
    });*/


    // Example slider

    /*var power = 0;*/

    jQuery('.filter .nav-tabs .nav-item:first-child a').addClass('active show');

    var activeTab = jQuery('.filter .nav-tabs .nav-item:first-child a').attr('href');
        /*activeTab = activeTab.slice(1);*/

    jQuery('.filter .tab-content '+activeTab+'').addClass('active show');

    jQuery('.tab-pane').each(function () {
        var $this = jQuery(this);
        var slider = jQuery(this).find('.ex-slider').attr('id');


        jQuery('.nav-tabs a').on('shown.bs.tab', function(event){
            jQuery('#' + slider).slick('reinit');

        });

        jQuery('#' + slider).slick({
            autoplay: false,
            lazyLoad: 'ondemand',
            infinite: false,
            autoplaySpeed: 5000,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true
        });


        jQuery(this).find('.slide-controls .prev').click(function(e){
            e.preventDefault();

            jQuery('#'+slider).slick('slickPrev');
        });

        jQuery(this).find('.slide-controls .next').click(function(e){
            e.preventDefault();

            jQuery('#'+slider).slick('slickNext');
        });

        var exSliderCount = jQuery('#'+slider+'').find('.filt-el').length;


        jQuery('#' + slider).find('.filt-el').each(function(i){
            jQuery(this).attr('data-index', i );
        });

        if( exSliderCount == 0){
            jQuery(this).find('.slide-controls').fadeOut(400);
        }else{
            jQuery(this).find('.slide-controls').fadeIn(400);
        }

        var progresData = 100/exSliderCount;

        $this.find('.slide-controls .slider-progress span').css({'width' : progresData+'%'});

        jQuery(this).find('.slide-controls .current').text(1);

        var nowExSliderNumber = 0;

        jQuery('#'+slider+'').on('afterChange', function(event, slick, currentSlide, nextSlide){

            jQuery('#' + slider).find('.filt-el').each(function(i){
                jQuery(this).attr('data-index', '');
                jQuery(this).attr('data-index', i);
            });

            nowExSliderNumber = 0;

            nowExSliderNumber = jQuery('#'+slider+'').find('.slick-current.slick-active').data('index');

            nowExSliderNumber = Number ( nowExSliderNumber + 1 );

            $this.find('.slide-controls .current').text(nowExSliderNumber);
            $this.find('.slide-controls .slider-progress span').css({'width' : (progresData * nowExSliderNumber)+'%'});

        });


        jQuery(this).find('.slide-controls .all').text(exSliderCount);

        //


        /*jQuery('#costRange').on('change', function () {
            power = jQuery(this).val();

            jQuery('#' + slider).slick('slickUnfilter');


            jQuery('.car-item').each(function () {
                var price = jQuery(this).data('price');
                var priceto = jQuery(this).data('priceto');

                jQuery(this).removeClass('filt-el');
                if( price < power ){
                    jQuery(this).addClass('filt-el');
                }
            });

            exSliderCount = jQuery('#'+slider+'').find('.filt-el').length;

            jQuery('#' + slider).find('.filt-el').each(function(i){
                jQuery(this).attr('data-index', '');
                jQuery(this).attr('data-index', i);
            });
            jQuery('#' + slider+'').slick('reinit');
            jQuery('#' + slider).slick('slickFilter', '.filt-el');

            jQuery('#' + slider).slick('slickGoTo', jQuery(this).find('.filt-el').data('index', 0));

            jQuery('#'+slider+'').on('afterChange', function(event, slick, currentSlide, nextSlide){

                jQuery('#'+slider+'').find('.filt-el:first-child').data('index', 0);

                nowExSliderNumber = Number ( currentSlide + 1);

                $this.find('.slide-controls .current').text(nowExSliderNumber);
                $this.find('.slide-controls .slider-progress span').css({'width' : (progresData * nowExSliderNumber)+'%'});

            });

            if( exSliderCount == 0){
                $this.find('.slide-controls').fadeOut(400);
            }else{
                $this.find('.slide-controls').fadeIn(400);
            }

            progresData = 100/exSliderCount;

            nowExSliderNumber = 1;

            $this.find('.slide-controls .current').text(1);

            $this.find('.slide-controls .slider-progress span').css({'width' : progresData +'%'});


            $this.find('.slide-controls .all').text(exSliderCount);

        });*/



        jQuery('.price-filter a').on('click', function (e) {
            e.preventDefault();

            jQuery('.price-filter a').removeClass('active-filter');

            jQuery(this).addClass('active-filter');

            var fname = jQuery(this).data('filtname');

            jQuery('#' + slider).slick('slickUnfilter');

            jQuery('.car-item').each(function () {
                var filerprice = jQuery(this).data('filerprice');

                jQuery(this).removeClass('filt-el');
                if( fname === filerprice ){
                    jQuery(this).addClass('filt-el');
                }
            });

            exSliderCount = jQuery('#'+slider+'').find('.filt-el').length;

            jQuery('#' + slider).find('.filt-el').each(function(i){
                jQuery(this).attr('data-index', '');
                jQuery(this).attr('data-index', i);
            });
            jQuery('#' + slider+'').slick('reinit');
            jQuery('#' + slider).slick('slickFilter', '.filt-el');

            jQuery('#' + slider).slick('slickGoTo', jQuery(this).find('.filt-el').data('index', 0));

            jQuery('#'+slider+'').on('afterChange', function(event, slick, currentSlide, nextSlide){

                jQuery('#'+slider+'').find('.filt-el:first-child').data('index', 0);

                nowExSliderNumber = Number ( currentSlide + 1);

                $this.find('.slide-controls .current').text(nowExSliderNumber);
                $this.find('.slide-controls .slider-progress span').css({'width' : (progresData * nowExSliderNumber)+'%'});

            });

            if( exSliderCount == 0){
                $this.find('.slide-controls').fadeOut(400);
            }else{
                $this.find('.slide-controls').fadeIn(400);
            }

            progresData = 100/exSliderCount;

            nowExSliderNumber = 1;

            $this.find('.slide-controls .current').text(1);

            $this.find('.slide-controls .slider-progress span').css({'width' : progresData +'%'});


            $this.find('.slide-controls .all').text(exSliderCount);


        });

    });

    // Main screen slider

    var timeMain = 5;
    var $barMain,
        $slickMain,
        isPauseMain,
        tickMain,
        percentTimeMain;

    $slickMain =  jQuery('#main-slider');

    $slickMain.slick({
        autoplay: false,
        lazyLoad: 'ondemand',
        autoplaySpeed: 5000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnFocus: false,
        pauseOnHover: true,
        arrows: false,
        fade: false,
        dots: true
    });

    $barMain = jQuery('.main-slider-info .time-progress span');

    jQuery('.main-slider-wrapper').on({
        mouseenter: function() {
            isPauseMain = true;
        },
        mouseleave: function() {
            isPauseMain = false;
        }
    });

    function startProgressbar() {
        resetProgressbar();
        percentTimeMain = 0;
        isPauseMain = false;
        tickMain = setInterval(interval, 10);
    }

    function interval() {
        if(isPauseMain === false) {
            percentTimeMain += 1 / (timeMain + 0.1);
            $barMain.css({
                width:percentTimeMain+"%"
            });
            if(percentTimeMain >= 100)
            {
                $slickMain.slick('slickNext');
                startProgressbar();
            }
        }
    }

    function resetProgressbar() {
        $barMain.css({
            width: 0+'%'
        });
        clearTimeout(tickMain);
    }

    startProgressbar();

    var mainSliderCount = jQuery('#main-slider .slide').length;
    var mainSliderClone = jQuery('#main-slider .slick-cloned').length;
        mainSliderCount = mainSliderCount - mainSliderClone;

    jQuery('.main-slider-info .current').text(1);

    var carMark = jQuery('#main-slider .slick-current').data('mark');
    var carModel = jQuery('#main-slider .slick-current').data('model');
    var carSave = jQuery('#main-slider .slick-current').data('save');
    var carYear = jQuery('#main-slider .slick-current').data('year');

    jQuery('.main-slider-info .car-name .mark').text(carMark);
    jQuery('.main-slider-info .car-name .model').text(carModel);
    jQuery('.main-slider-info .saving .text p').text(carSave);
    jQuery('.main-slider-info .car-year .text p').text(carYear);

    jQuery('#main-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){

        startProgressbar();

        var nowMainSliderNumber = jQuery('#main-slider .slick-current').data('slick-index');
        var carMark = jQuery('#main-slider .slick-current').data('mark');
        var carModel = jQuery('#main-slider .slick-current').data('model');
        var carSave = jQuery('#main-slider .slick-current').data('save');
        var carYear = jQuery('#main-slider .slick-current').data('year');

        nowMainSliderNumber = Number ( nowMainSliderNumber + 1 );

        jQuery('.main-slider-info .current').text(nowMainSliderNumber);
        jQuery('.main-slider-info .car-name .mark').text(carMark);
        jQuery('.main-slider-info .car-name .model').text(carModel);
        jQuery('.main-slider-info .saving .text p').text(carSave);
        jQuery('.main-slider-info .car-year .text p').text(carYear);

    });


    jQuery('.main-slider-info .all').text(mainSliderCount);

    // Delivered slider

    var timeDelivered = 15;
    var $barDelivered,
        $slickDelivered,
        isPauseDelivered,
        tickDelivered,
        percentTimeDelivered;

    $slickDelivered = jQuery('#delivered-slider');

    $slickDelivered.slick({
        autoplay: false,
        lazyLoad: 'ondemand',
        autoplaySpeed: 15000,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnFocus: false,
        pauseOnHover: true,
        arrows: false,
        fade: true
    });

    $barDelivered = jQuery('.delivered-cars .slider-progress span');

    jQuery('.delivered-slider').on({
        mouseenter: function() {
            isPauseDelivered = true;
        },
        mouseleave: function() {
            isPauseDelivered = false;
        }
    });

    function startProgressbar2() {
        resetProgressbar2();
        percentTimeDelivered = 0;
        isPauseDelivered = false;
        tickDelivered = setInterval(interval2, 10);
    }

    function interval2() {
        if(isPauseDelivered === false) {
            percentTimeDelivered += 1 / (timeDelivered + 0.1);
            $barDelivered.css({
                width: percentTimeDelivered+"%"
            });
            if(percentTimeDelivered >= 100)
            {
                $slickDelivered.slick('slickNext');
                startProgressbar2();
            }
        }
    }

    function resetProgressbar2() {
        $barDelivered.css({
            width: 0+'%'
        });
        clearTimeout(tickDelivered);
    }

    startProgressbar2();

    jQuery('.delivered-cars .controls-wrapper .prev').click(function(e){
        e.preventDefault();
        startProgressbar2();
        jQuery('#delivered-slider').slick('slickPrev');

    });

    jQuery('.delivered-cars .controls-wrapper .next').click(function(e){
        e.preventDefault();
        startProgressbar2();
        jQuery('#delivered-slider').slick('slickNext');

    });


    var deliveredSliderCount = jQuery('#delivered-slider .slide').length;

    var deliveredSlideClone = jQuery('#delivered-slider .slick-cloned').length;
    deliveredSliderCount = deliveredSliderCount - deliveredSlideClone;

    jQuery('.delivered-cars .slider-controls .all').text(deliveredSliderCount);

    jQuery('.delivered-cars .slider-controls .current').text(1);

    jQuery('#delivered-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){

        startProgressbar2();

        var nowDeliveredSliderNumber = jQuery('#delivered-slider .slick-current').data('slick-index');

        nowDeliveredSliderNumber = Number ( nowDeliveredSliderNumber + 1 );

        jQuery('.delivered-cars .slider-controls .current').text(nowDeliveredSliderNumber);

        jQuery(this).find('video').each(function () {
           jQuery(this).get(0).pause();
            jQuery('.delivered-slider .control-video').fadeIn(400);
        });

    });

    jQuery('.delivered-slider .slide').each(function () {

        var thisSlide = jQuery(this);

        var sleVideo = thisSlide.find('video');

        thisSlide.find('.control-video').on('click', function (e) {
            e.preventDefault();

            jQuery(this).fadeOut(400);
            sleVideo.get(0).play();

        })

    });

    jQuery('[data-fancybox]').fancybox({
        protect: true,
        loop: true,
        infobar: false
    });

    // UTM

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }
    }

    utm_source = getQueryVariable('utm_source') ? getQueryVariable('utm_source') : "";
    utm_medium = getQueryVariable('utm_medium') ? getQueryVariable('utm_medium') : "";
    utm_campaign = getQueryVariable('utm_campaign') ? getQueryVariable('utm_campaign') : "";
    utm_term = getQueryVariable('utm_term') ? getQueryVariable('utm_term') : "";
    utm_content = getQueryVariable('utm_content') ? getQueryVariable('utm_content') : "";

    var forms = jQuery('form');

    jQuery.each(forms, function (index, form) {
        jQueryform = jQuery(form);
        jQueryform.append('<input type="hidden" name="utm_source" value="' + utm_source + '">');
        jQueryform.append('<input type="hidden" name="utm_medium" value="' + utm_medium + '">');
        jQueryform.append('<input type="hidden" name="utm_campaign" value="' + utm_campaign + '">');
        jQueryform.append('<input type="hidden" name="utm_term" value="' + utm_term + '">');
        jQueryform.append('<input type="hidden" name="utm_content" value="' + utm_content + '">');
    });

    // MAP INIT

    var lat = jQuery('#map').data('lat');
    var lng = jQuery('#map').data('lng');

    function initMap() {
        var location = {
            lat: lat,
            lng: lng
        };

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: location,
            scrollwheel: false,
            disableDefaultUI: true
        });

        var marker = new google.maps.Marker({ // кастомный марекр + подпись
            position: location,
            map: map,
            icon: {
                url: ('http://woom-test.smmstudio.com.ua/wp-content/themes/woom/assets/img/map-marker.png'),
                scaledSize: new google.maps.Size(45, 45)
            }
        });

        map.setOptions({
            styles:
                [
                    {
                        "featureType": "administrative",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#444444"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [
                            {
                                "saturation": -100
                            },
                            {
                                "lightness": 45
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "color": "#7f75b5"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    }
                ]
        });

    }

    if( jQuery('#contacts').length ){
        initMap();
    }

    //odometrr options
    window.odometerOptions = {
        auto: false,
        duration: 3000
    };

    var num1 = jQuery('.our-numbers .item:nth-child(2) .number').data('number');
    var num2 = jQuery('.our-numbers .item:nth-child(3) .number').data('number');
    var num3 = jQuery('.our-numbers .item:nth-child(4) .number').data('number');

    var OdometrPosition = jQuery('.our-numbers');

    OdometrPosition.viewportChecker({

        offset: 200,

        callbackFunction: function (elem, action) {

            setTimeout(function() {
                jQuery('.our-numbers .item:nth-child(2) .number').text(num1);
                jQuery('.our-numbers .item:nth-child(3) .number').text(num2);
                jQuery('.our-numbers .item:nth-child(4) .number').text(num3);
            }, 200);

            setTimeout(function () {
                jQuery('.our-numbers .item .number').fadeOut(400);
            }, 1500);

            setTimeout(function () {
                jQuery('.our-numbers .error-in').addClass('up-error');
            }, 1500);

            setTimeout(function () {
                jQuery('.our-numbers .slogan .inner').addClass('in-animate');
            }, 1500);

            setTimeout(function () {
                jQuery('.delivered-cars').addClass('animate-h');
            }, 1500);

        }
    });

    // Animation Visible

    var animateSimple = jQuery('.buy-auto-simple .animate');
    var animateAdvantages = jQuery('.advantages .animate');
    var animateResponsibility = jQuery('.responsibility .animate');
    var animateKnowyourself = jQuery('.know-yourself');

    var startAnimationDelay = 200;

    animateSimple.viewportChecker({

        offset: startAnimationDelay,

        callbackFunction: function (elem, action) {

            jQuery('.visible').addClass('animation-up');

        }
    });

    animateAdvantages.viewportChecker({

        offset: startAnimationDelay,

        callbackFunction: function (elem, action) {

            jQuery('.visible').addClass('animation-up');

        }
    });

    animateResponsibility.viewportChecker({

        offset: startAnimationDelay,

        callbackFunction: function (elem, action) {

            jQuery('.visible').addClass('animation-up');

        }
    });

    animateKnowyourself.viewportChecker({

        offset: startAnimationDelay,

        callbackFunction: function (elem, action) {

            jQuery('.visible .item').addClass('animation');

        }
    });

    // Lazy load

    jQuery('.lazy').lazy();

});

