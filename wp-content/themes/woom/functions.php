<?php
/**
 * woom functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package woom
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'woom_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function woom_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on woom, use a find and replace
		 * to change 'woom' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'woom', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'woom' ),
                'menu-2' => esc_html__( 'Primary ru', 'woom' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'woom_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'woom_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function woom_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'woom_content_width', 640 );
}
add_action( 'after_setup_theme', 'woom_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Enqueue scripts and styles.
 */

require get_template_directory() . '/inc/scripts-style.php';

/**
 * Custom post types.
 */

require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 * Redax options.
 */
require get_template_directory() . '/inc/site-options.php';

    define( 'SITE_URL', get_site_url() );
    define( 'SITE_LOCALE', get_locale() );
    define( 'THEME_PATH', get_template_directory_uri() );

//


function prefix_send_email_to_admin() {
    require_once __DIR__ . '/amocrm.phar';

    require_once  'Mobile_Detect.php';

    $detect = new Mobile_Detect;

    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $massage = $_POST['massage'];

    $utmSource = $_POST['utm_source'];
    $utmMedium = $_POST['utm_medium'];
    $utmCampaign = $_POST['utm_campaign'];
    $utmTerm = $_POST['utm_term'];
    $utmContent = $_POST['utm_content'];

    $allUtm = 'Source '.$utmSource.', Medium '.$utmMedium.', Campaign '.$utmCampaign.', Term '.$utmTerm.', Content '.$utmContent.'';

    $lang = $_POST['lang'];
    $home = $_POST['home'];
    $thx = 'thx';

    if( $lang == 'ru'){
        $thx = 'thx-2';
    }

    $mobOs = '';
    $device = '225473';

    // Check for a specific platform with the help of the magic methods:
    if( $detect->isiOS() ){
        $mobOs = '225511';
    }

    if( $detect->isAndroidOS() ){
        $mobOs = '225513';
    }

    if( $mobOs != ''){
        $device = '225471';
    }

    try {

        // Создание клиента
        $subdomain = 'infowoomcarscom';            // Поддомен в амо срм
        $login     = 'info@woomcars.com';            // Логин в амо срм
        $apikey    = '5947e7957e76add18d7837c2afd5ac76a07281ed';            // api ключ


        $amo = new \AmoCRM\Client($subdomain, $login, $apikey);

        // Вывести полученые из амо данные
         /*echo '<pre>';
         print_r($amo->account->apiCurrent());
         echo '</pre>';*/

        // создаем лида
        $lead = $amo->lead;
        $lead['name'] = 'Зявка с сайта';
        $lead['status_id'] = 32696716;

        $lead->addCustomField(127971, '224043'); // Источник
        $lead->addCustomField(129885, '226657' ); // Продукт
        $lead->addCustomField(129091, $mobOs );
        $lead->addCustomField(129069, $device );

        /*$lead->addCustomField(386177, $utmContent );

        $lead->addCustomMultiField(612953, [ $package ]);*/

        $lead->addCustomField(272265, $allUtm );
        /*$lead->addCustomField(386171, $utmMedium );
        $lead->addCustomField(386173, $utmCampaign );
        $lead->addCustomField(386175, $utmTerm );
        $lead->addCustomField(386177, $utmContent );*/


        $lidId = $lead->apiAdd();

        $note = $amo->note;
        $note['element_id'] = $lidId;
        $note['element_type'] = \AmoCRM\Models\Note::TYPE_LEAD;
        $note['note_type'] = \AmoCRM\Models\Note::COMMON;
        $note['text'] = $massage;
        $note->apiAdd();


        // Получение экземпляра модели для работы с контактами
        $contact = $amo->contact;

        $rest = substr($phone, 8);

        /*echo $phone;
        echo $rest;*/

        $currentUser = $contact->apiList([
            'query' => $rest,
            'limit_rows' => 1,
        ]);

        /*$contPhone = $currentUser[0]['custom_fields'][0]['values'][0]['value'];
        $myPhone = preg_replace( '/[^0-9]/', '', $contPhone );*/
        /*echo '<pre>';*/
        /*$contPhone;*/ /*= $currentUser[0]['custom_fields'][0]['values'][0]['value'];*/
        /*print_r($currentUser[0]['custom_fields'][0]['values'][0]['value']);*/

        /*echo '</pre>';
        echo $myPhone;
        echo $phone;*/

        $id = $currentUser[0]['id'];
        $linkId = $currentUser[0]['linked_leads_id'];

        $allLInks[] = $lidId;

        foreach ($linkId as $item) {
            $allLInks[] = (string)$item;
        }

        if(!empty($currentUser)){

            $contact['linked_leads_id'] = $allLInks;
            /*$contact->addCustomField(127931, [
                [$phone, 'MOB'],
            ]);*/

            $contact->apiUpdate((int)$id, 'now');

        } else{
            // Заполнение полей модели
            $contact['name'] = isset($name) ? $name : 'Не указано';
            $contact['linked_leads_id'] = [(int)$lidId];

            $contact->addCustomField(127931, [
                [$phone, 'MOB'],
            ]);

            $id = $contact->apiAdd();
        }


    } catch (\AmoCRM\Exception $e) {
        printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
    }

    header('Location: '.$home.$thx.'');
}
add_action( 'admin_post_nopriv_contact_form', 'prefix_send_email_to_admin' );
add_action( 'admin_post_contact_form', 'prefix_send_email_to_admin' );




// car filter

    add_action('pre_get_posts', 'bda_filters');
    function bda_filters($query)
    {
        $id = get_the_ID();
        $postMeta = get_post_meta($id);

        if ($query->is_main_query()) {

            if (isset($_GET['category']) != '' && $_GET['category'] != 'all' && $_GET['category'] != 0) {


                if ('auto_examples' == get_query_var('post_type')) {
                    $query->set('tax_query', array(
                        array(
                            'taxonomy' => 'auto-examples-tax',
                            'field' => 'slug',
                            'terms' => array($_GET['category']),
                        )
                    ));
                }
            }

            if (isset($_GET['price']) && $_GET['price']) {
                $query->set('meta_query', array(
                    array(
                        'key' => 'price',
                        'value' => explode(',', $_GET['price']),

                    )
                ));
            }
            return;
        }


    }