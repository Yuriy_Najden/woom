<?php
    /**
     * Template part for thx page
     *
     * Template name: Шаблон страницы спасибо
     *
     * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
     *
     * @package woom
     */

    $url = get_page_uri();

    if( $url == 'thx' ){
        get_header();
    }else{
        get_header('ru');
    }


?>

    <main>
<?php
    while ( have_posts() ) :
        the_post();
        ?>
        <section class="thx-page">
            <div class="container">
                <div class="row">
                    <div class="content col-12">
                        <div class="text">
                            <?php the_content();?>
                            <?php
                                /*$url = get_page_uri();*/

                                if( $url == 'thx' ){;
                            ?>
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="button">
                                        Повернутися на сайт
                                    </a>
                            <?php }else{;?>
                                    <a href="<?php echo esc_url( home_url( '/ru' ) ); ?>" class="button">
                                        Вернуться на сайт
                                    </a>
                            <?php };?>
                        </div>
                        <img src="<?php the_post_thumbnail_url();?>" alt="">
                    </div>
                </div>
            </div>
        </section>
        <?php
    endwhile;
?>
    </main>

<?php get_footer(); ?>