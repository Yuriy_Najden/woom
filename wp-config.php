<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'woom');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '14122013');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x79r]~?pP~bRa/cBhs^>|@,Lo#yWOL74MTb[m[)fvb_59j7-3/pvdLo%-=x-37&m');
define('SECURE_AUTH_KEY',  '{U0JFh[gU<MZj,^i`;IFu9!UiZhC;Ks}t4<&a*!XT895`!){(]|wf~+T!VXxZ1j3');
define('LOGGED_IN_KEY',    'J;`M/{xUcHJKa~kOPj}Dn@pwCINr2})IFCYPRbM 8T~,~a*tEIUK!`HI(ufT54n-');
define('NONCE_KEY',        '(WEikz4}DMyh+q}o(L.2`=iPFTs)+g2?gK%ZmU}]+-2fCrtB5/iGO4EE(Z:g8Vf2');
define('AUTH_SALT',        'r=r1I BJvgc{&w&AQ<L^A{BZ|D>*.?Cyd5OhLH?V0?@>%bXP$CrU[x_>cqhg^HFI');
define('SECURE_AUTH_SALT', 'SjMZVFBSM!%z?4G~3ycx:VyHe,$R;WGs0E}_ %xmzMW:hWt+6+ta1*mmGy:q@rlI');
define('LOGGED_IN_SALT',   'nmcuW+RYXSvt+[^T(E>vKlx|S{7D3q(4APc<1K;2Gdi)]4=T-rEBhuj3X!QrDJFx');
define('NONCE_SALT',       'zTpbKQ,*G#BM]p+@n8;pVkRr?YhfdO%T4ztEgU!{MT@ro5xEs4F<k=Y]YvKmlx9o');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'autowoomdb_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');

/** Загрузка плагинов и обновлений без FTP. */
define('FS_METHOD', 'direct');

/* Отключение редактирования файлов в админке WP */
define('DISALLOW_FILE_EDIT', true);